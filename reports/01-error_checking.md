

```python
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from dateutil.parser import parse
from datetime import date

DATECOLS = ['INIZIO_PUBBLICITA', 'FINE_PUBBLICITA', 'DATA']
```


```python
dataset = pd.read_csv('../data/raw/train.csv')
```


```python
dataset.head()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PUBBLICITA</th>
      <th>INIZIO_PUBBLICITA</th>
      <th>FINE_PUBBLICITA</th>
      <th>MACRO_TIPO_PUBBLICITA</th>
      <th>FASCIA_ORARIA</th>
      <th>FASCIA_TEMPISTICA</th>
      <th>TIPO_PUBBLICITA</th>
      <th>DATA</th>
      <th>CODICE_GIOCO</th>
      <th>RILANCIO_PUBBLICITA</th>
      <th>SESSIONE</th>
      <th>NUMERO_CLICK_SITO</th>
      <th>OFFERTA_PROMOZIONALE</th>
      <th>MACRO_CATEGORIA_GIOCO</th>
      <th>MICRO_CATEGORIA_GIOCO</th>
      <th>AREA_CLICK</th>
      <th>NUMERO_PUBBLICITA_CONCORRENTI</th>
      <th>ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>279.0</td>
      <td>2016-04-01</td>
      <td>2016-04-23</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>2016-04-07</td>
      <td>461</td>
      <td>30</td>
      <td>121941195</td>
      <td>5510</td>
      <td>0.100</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>85091</td>
    </tr>
    <tr>
      <th>1</th>
      <td>286.0</td>
      <td>2016-04-01</td>
      <td>2016-04-23</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>2016-04-14</td>
      <td>761</td>
      <td>161</td>
      <td>122582337</td>
      <td>5150</td>
      <td>0.182</td>
      <td>3</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>18118</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.0</td>
      <td>2000-01-01</td>
      <td>2030-12-31</td>
      <td>6</td>
      <td>4</td>
      <td>4</td>
      <td>3.0</td>
      <td>2016-02-10</td>
      <td>628</td>
      <td>174</td>
      <td>117101149</td>
      <td>5510</td>
      <td>0.000</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>129575</td>
    </tr>
    <tr>
      <th>3</th>
      <td>699.0</td>
      <td>2016-06-16</td>
      <td>2016-08-02</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-07-28</td>
      <td>363</td>
      <td>179</td>
      <td>134105783</td>
      <td>5160</td>
      <td>0.231</td>
      <td>3</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>38961</td>
    </tr>
    <tr>
      <th>4</th>
      <td>473.0</td>
      <td>2000-01-01</td>
      <td>2016-05-02</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>105</td>
      <td>168</td>
      <td>124139895</td>
      <td>5239</td>
      <td>0.220</td>
      <td>2</td>
      <td>1</td>
      <td>114</td>
      <td>4</td>
      <td>133839</td>
    </tr>
  </tbody>
</table>
</div>




```python
dataset.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 127888 entries, 0 to 127887
    Data columns (total 18 columns):
    PUBBLICITA                       127148 non-null float64
    INIZIO_PUBBLICITA                127888 non-null object
    FINE_PUBBLICITA                  127888 non-null object
    MACRO_TIPO_PUBBLICITA            127888 non-null int64
    FASCIA_ORARIA                    127888 non-null int64
    FASCIA_TEMPISTICA                127888 non-null int64
    TIPO_PUBBLICITA                  127368 non-null float64
    DATA                             124729 non-null object
    CODICE_GIOCO                     127888 non-null int64
    RILANCIO_PUBBLICITA              127888 non-null int64
    SESSIONE                         127888 non-null int64
    NUMERO_CLICK_SITO                127888 non-null int64
    OFFERTA_PROMOZIONALE             127888 non-null float64
    MACRO_CATEGORIA_GIOCO            127888 non-null int64
    MICRO_CATEGORIA_GIOCO            127888 non-null int64
    AREA_CLICK                       127888 non-null int64
    NUMERO_PUBBLICITA_CONCORRENTI    127888 non-null int64
    ID                               127888 non-null int64
    dtypes: float64(3), int64(12), object(3)
    memory usage: 17.6+ MB



```python
dataset.describe()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PUBBLICITA</th>
      <th>MACRO_TIPO_PUBBLICITA</th>
      <th>FASCIA_ORARIA</th>
      <th>FASCIA_TEMPISTICA</th>
      <th>TIPO_PUBBLICITA</th>
      <th>CODICE_GIOCO</th>
      <th>RILANCIO_PUBBLICITA</th>
      <th>SESSIONE</th>
      <th>NUMERO_CLICK_SITO</th>
      <th>OFFERTA_PROMOZIONALE</th>
      <th>MACRO_CATEGORIA_GIOCO</th>
      <th>MICRO_CATEGORIA_GIOCO</th>
      <th>AREA_CLICK</th>
      <th>NUMERO_PUBBLICITA_CONCORRENTI</th>
      <th>ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>127148.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127368.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>1.278880e+05</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.0</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>326.203330</td>
      <td>5.816378</td>
      <td>2.509805</td>
      <td>2.776203</td>
      <td>3.964858</td>
      <td>392.297940</td>
      <td>94.743635</td>
      <td>1.235237e+08</td>
      <td>5505.730616</td>
      <td>0.156185</td>
      <td>1.986535</td>
      <td>1.0</td>
      <td>5.113865</td>
      <td>1.733009</td>
      <td>83920.230686</td>
    </tr>
    <tr>
      <th>std</th>
      <td>280.481683</td>
      <td>1.835892</td>
      <td>1.917565</td>
      <td>2.181093</td>
      <td>1.160202</td>
      <td>221.969084</td>
      <td>51.401158</td>
      <td>4.188625e+06</td>
      <td>579.002292</td>
      <td>0.136981</td>
      <td>0.454355</td>
      <td>0.0</td>
      <td>15.235427</td>
      <td>3.506405</td>
      <td>48428.294332</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.163034e+08</td>
      <td>-100.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>1.0</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>108.000000</td>
      <td>6.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>3.000000</td>
      <td>199.000000</td>
      <td>53.000000</td>
      <td>1.198935e+08</td>
      <td>5340.000000</td>
      <td>0.000000</td>
      <td>2.000000</td>
      <td>1.0</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>41950.750000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>210.000000</td>
      <td>7.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>4.000000</td>
      <td>440.000000</td>
      <td>99.000000</td>
      <td>1.236580e+08</td>
      <td>5472.000000</td>
      <td>0.149000</td>
      <td>2.000000</td>
      <td>1.0</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>84000.500000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>618.000000</td>
      <td>7.000000</td>
      <td>4.000000</td>
      <td>4.000000</td>
      <td>5.000000</td>
      <td>595.000000</td>
      <td>142.000000</td>
      <td>1.268844e+08</td>
      <td>5660.000000</td>
      <td>0.243000</td>
      <td>2.000000</td>
      <td>1.0</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>125822.250000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>840.000000</td>
      <td>8.000000</td>
      <td>6.000000</td>
      <td>8.000000</td>
      <td>7.000000</td>
      <td>807.000000</td>
      <td>181.000000</td>
      <td>1.349159e+08</td>
      <td>15050.000000</td>
      <td>2.778000</td>
      <td>3.000000</td>
      <td>1.0</td>
      <td>115.000000</td>
      <td>37.000000</td>
      <td>167839.000000</td>
    </tr>
  </tbody>
</table>
</div>



# NA checking

Check if exists a value in each column


```python
dataset.isnull().any()
```




    PUBBLICITA                        True
    INIZIO_PUBBLICITA                False
    FINE_PUBBLICITA                  False
    MACRO_TIPO_PUBBLICITA            False
    FASCIA_ORARIA                    False
    FASCIA_TEMPISTICA                False
    TIPO_PUBBLICITA                   True
    DATA                              True
    CODICE_GIOCO                     False
    RILANCIO_PUBBLICITA              False
    SESSIONE                         False
    NUMERO_CLICK_SITO                False
    OFFERTA_PROMOZIONALE             False
    MACRO_CATEGORIA_GIOCO            False
    MICRO_CATEGORIA_GIOCO            False
    AREA_CLICK                       False
    NUMERO_PUBBLICITA_CONCORRENTI    False
    ID                               False
    dtype: bool




```python
null_pubblicita = dataset['PUBBLICITA'].isnull().sum()
null_tipo_pubblicita = dataset['TIPO_PUBBLICITA'].isnull().sum()
null_data = dataset['DATA'].isnull().sum()
print(f'#null pubblicita: {null_pubblicita},\n#null tipo pubblicita: {null_tipo_pubblicita}\n#null data: {null_data}')
```

    #null pubblicita: 740,
    #null tipo pubblicita: 520
    #null data: 3159


Find the rows with null values


```python
dataset[dataset.isnull().any(axis=1)]
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PUBBLICITA</th>
      <th>INIZIO_PUBBLICITA</th>
      <th>FINE_PUBBLICITA</th>
      <th>MACRO_TIPO_PUBBLICITA</th>
      <th>FASCIA_ORARIA</th>
      <th>FASCIA_TEMPISTICA</th>
      <th>TIPO_PUBBLICITA</th>
      <th>DATA</th>
      <th>CODICE_GIOCO</th>
      <th>RILANCIO_PUBBLICITA</th>
      <th>SESSIONE</th>
      <th>NUMERO_CLICK_SITO</th>
      <th>OFFERTA_PROMOZIONALE</th>
      <th>MACRO_CATEGORIA_GIOCO</th>
      <th>MICRO_CATEGORIA_GIOCO</th>
      <th>AREA_CLICK</th>
      <th>NUMERO_PUBBLICITA_CONCORRENTI</th>
      <th>ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>4</th>
      <td>473.0</td>
      <td>2000-01-01</td>
      <td>2016-05-02</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>105</td>
      <td>168</td>
      <td>124139895</td>
      <td>5239</td>
      <td>0.220</td>
      <td>2</td>
      <td>1</td>
      <td>114</td>
      <td>4</td>
      <td>133839</td>
    </tr>
    <tr>
      <th>21</th>
      <td>189.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>37</td>
      <td>139</td>
      <td>118691106</td>
      <td>5510</td>
      <td>0.100</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>7</td>
      <td>148664</td>
    </tr>
    <tr>
      <th>61</th>
      <td>NaN</td>
      <td>2016-03-10</td>
      <td>2016-03-28</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>2016-03-18</td>
      <td>277</td>
      <td>132</td>
      <td>120390529</td>
      <td>5560</td>
      <td>0.091</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>13038</td>
    </tr>
    <tr>
      <th>122</th>
      <td>189.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>37</td>
      <td>11</td>
      <td>118661337</td>
      <td>5510</td>
      <td>0.100</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>7</td>
      <td>148611</td>
    </tr>
    <tr>
      <th>128</th>
      <td>734.0</td>
      <td>2016-06-19</td>
      <td>2016-07-01</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>40</td>
      <td>103</td>
      <td>128197386</td>
      <td>5609</td>
      <td>0.154</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>107500</td>
    </tr>
    <tr>
      <th>157</th>
      <td>NaN</td>
      <td>2016-06-13</td>
      <td>2016-06-14</td>
      <td>2</td>
      <td>6</td>
      <td>7</td>
      <td>4.0</td>
      <td>2016-06-13</td>
      <td>542</td>
      <td>71</td>
      <td>126846957</td>
      <td>5427</td>
      <td>0.388</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>102782</td>
    </tr>
    <tr>
      <th>179</th>
      <td>732.0</td>
      <td>2016-06-19</td>
      <td>2016-07-01</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>683</td>
      <td>18</td>
      <td>128189389</td>
      <td>5529</td>
      <td>0.147</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>106956</td>
    </tr>
    <tr>
      <th>185</th>
      <td>187.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>105</td>
      <td>177</td>
      <td>118589138</td>
      <td>5239</td>
      <td>0.222</td>
      <td>2</td>
      <td>1</td>
      <td>24</td>
      <td>1</td>
      <td>134452</td>
    </tr>
    <tr>
      <th>190</th>
      <td>1.0</td>
      <td>2000-01-01</td>
      <td>2030-12-31</td>
      <td>6</td>
      <td>4</td>
      <td>4</td>
      <td>3.0</td>
      <td>NaN</td>
      <td>659</td>
      <td>40</td>
      <td>118605399</td>
      <td>5430</td>
      <td>0.000</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>117814</td>
    </tr>
    <tr>
      <th>193</th>
      <td>120.0</td>
      <td>2016-02-01</td>
      <td>2016-03-02</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>786</td>
      <td>55</td>
      <td>118597378</td>
      <td>5330</td>
      <td>0.229</td>
      <td>3</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>65589</td>
    </tr>
    <tr>
      <th>198</th>
      <td>NaN</td>
      <td>2016-07-05</td>
      <td>2016-07-18</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-07-11</td>
      <td>92</td>
      <td>23</td>
      <td>128957950</td>
      <td>5148</td>
      <td>0.200</td>
      <td>3</td>
      <td>1</td>
      <td>28</td>
      <td>1</td>
      <td>130234</td>
    </tr>
    <tr>
      <th>238</th>
      <td>176.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>464</td>
      <td>131</td>
      <td>118680034</td>
      <td>-100</td>
      <td>0.243</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>78928</td>
    </tr>
    <tr>
      <th>241</th>
      <td>176.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>464</td>
      <td>121</td>
      <td>118631249</td>
      <td>5340</td>
      <td>0.243</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>78786</td>
    </tr>
    <tr>
      <th>264</th>
      <td>176.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>464</td>
      <td>48</td>
      <td>118666848</td>
      <td>5340</td>
      <td>0.243</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>77648</td>
    </tr>
    <tr>
      <th>279</th>
      <td>179.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>282</td>
      <td>11</td>
      <td>118601277</td>
      <td>5440</td>
      <td>0.050</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>79784</td>
    </tr>
    <tr>
      <th>281</th>
      <td>443.0</td>
      <td>2000-01-01</td>
      <td>2016-11-08</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>655</td>
      <td>108</td>
      <td>124133951</td>
      <td>5707</td>
      <td>0.353</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>91035</td>
    </tr>
    <tr>
      <th>296</th>
      <td>176.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>464</td>
      <td>55</td>
      <td>118597498</td>
      <td>5340</td>
      <td>0.243</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>77756</td>
    </tr>
    <tr>
      <th>371</th>
      <td>179.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>282</td>
      <td>131</td>
      <td>118586603</td>
      <td>5440</td>
      <td>0.050</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>80060</td>
    </tr>
    <tr>
      <th>451</th>
      <td>NaN</td>
      <td>2016-03-10</td>
      <td>2016-03-28</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>2016-03-24</td>
      <td>469</td>
      <td>94</td>
      <td>121098295</td>
      <td>5660</td>
      <td>0.077</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>13716</td>
    </tr>
    <tr>
      <th>460</th>
      <td>344.0</td>
      <td>2016-04-24</td>
      <td>2016-05-06</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>483</td>
      <td>165</td>
      <td>124138994</td>
      <td>5559</td>
      <td>0.168</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>21814</td>
    </tr>
    <tr>
      <th>463</th>
      <td>91.0</td>
      <td>2016-02-02</td>
      <td>2016-09-11</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>459</td>
      <td>96</td>
      <td>118623333</td>
      <td>5610</td>
      <td>0.154</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>63910</td>
    </tr>
    <tr>
      <th>471</th>
      <td>735.0</td>
      <td>2016-06-19</td>
      <td>2016-07-01</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>462</td>
      <td>99</td>
      <td>133240181</td>
      <td>5410</td>
      <td>0.222</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>107858</td>
    </tr>
    <tr>
      <th>488</th>
      <td>173.0</td>
      <td>2016-03-10</td>
      <td>2016-03-28</td>
      <td>3</td>
      <td>6</td>
      <td>5</td>
      <td>NaN</td>
      <td>2016-03-21</td>
      <td>683</td>
      <td>69</td>
      <td>120782726</td>
      <td>5560</td>
      <td>0.091</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>73708</td>
    </tr>
    <tr>
      <th>495</th>
      <td>516.0</td>
      <td>2000-01-01</td>
      <td>2016-05-02</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>463</td>
      <td>88</td>
      <td>124136072</td>
      <td>5450</td>
      <td>0.220</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>24539</td>
    </tr>
    <tr>
      <th>539</th>
      <td>1.0</td>
      <td>2000-01-01</td>
      <td>2030-12-31</td>
      <td>6</td>
      <td>4</td>
      <td>4</td>
      <td>3.0</td>
      <td>NaN</td>
      <td>655</td>
      <td>150</td>
      <td>118725569</td>
      <td>5870</td>
      <td>0.000</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>127661</td>
    </tr>
    <tr>
      <th>563</th>
      <td>176.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>464</td>
      <td>132</td>
      <td>118675261</td>
      <td>5340</td>
      <td>0.243</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>78937</td>
    </tr>
    <tr>
      <th>569</th>
      <td>130.0</td>
      <td>2016-02-02</td>
      <td>2016-02-25</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>479</td>
      <td>123</td>
      <td>118680237</td>
      <td>5460</td>
      <td>0.245</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>67574</td>
    </tr>
    <tr>
      <th>575</th>
      <td>322.0</td>
      <td>2016-04-10</td>
      <td>2016-04-12</td>
      <td>2</td>
      <td>2</td>
      <td>7</td>
      <td>NaN</td>
      <td>2016-04-12</td>
      <td>690</td>
      <td>55</td>
      <td>122486919</td>
      <td>5605</td>
      <td>0.273</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>86971</td>
    </tr>
    <tr>
      <th>612</th>
      <td>187.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>105</td>
      <td>60</td>
      <td>118685132</td>
      <td>5239</td>
      <td>0.222</td>
      <td>2</td>
      <td>1</td>
      <td>24</td>
      <td>1</td>
      <td>134181</td>
    </tr>
    <tr>
      <th>626</th>
      <td>187.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>105</td>
      <td>65</td>
      <td>118683312</td>
      <td>5239</td>
      <td>0.222</td>
      <td>2</td>
      <td>1</td>
      <td>24</td>
      <td>1</td>
      <td>134194</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>126999</th>
      <td>733.0</td>
      <td>2016-06-19</td>
      <td>2016-07-01</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>553</td>
      <td>140</td>
      <td>128209554</td>
      <td>5660</td>
      <td>0.143</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>107182</td>
    </tr>
    <tr>
      <th>127013</th>
      <td>90.0</td>
      <td>2016-02-02</td>
      <td>2016-03-10</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>456</td>
      <td>132</td>
      <td>118626249</td>
      <td>5810</td>
      <td>0.118</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>63799</td>
    </tr>
    <tr>
      <th>127132</th>
      <td>1.0</td>
      <td>2000-01-01</td>
      <td>2030-12-31</td>
      <td>6</td>
      <td>4</td>
      <td>4</td>
      <td>3.0</td>
      <td>NaN</td>
      <td>389</td>
      <td>1</td>
      <td>118667546</td>
      <td>5360</td>
      <td>0.000</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>115340</td>
    </tr>
    <tr>
      <th>127153</th>
      <td>742.0</td>
      <td>2016-06-19</td>
      <td>2016-07-01</td>
      <td>2</td>
      <td>6</td>
      <td>3</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>199</td>
      <td>175</td>
      <td>128212840</td>
      <td>5560</td>
      <td>0.576</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>108945</td>
    </tr>
    <tr>
      <th>127158</th>
      <td>385.0</td>
      <td>2000-01-01</td>
      <td>2020-10-30</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>268</td>
      <td>70</td>
      <td>124116438</td>
      <td>5473</td>
      <td>0.220</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>23249</td>
    </tr>
    <tr>
      <th>127180</th>
      <td>187.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>105</td>
      <td>91</td>
      <td>118623025</td>
      <td>5239</td>
      <td>0.222</td>
      <td>2</td>
      <td>1</td>
      <td>24</td>
      <td>1</td>
      <td>134250</td>
    </tr>
    <tr>
      <th>127183</th>
      <td>422.0</td>
      <td>2000-01-01</td>
      <td>2020-10-30</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>348</td>
      <td>14</td>
      <td>124121768</td>
      <td>5605</td>
      <td>0.394</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>23438</td>
    </tr>
    <tr>
      <th>127192</th>
      <td>176.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>464</td>
      <td>69</td>
      <td>118735742</td>
      <td>5340</td>
      <td>0.243</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>78008</td>
    </tr>
    <tr>
      <th>127209</th>
      <td>393.0</td>
      <td>2000-01-01</td>
      <td>2020-10-30</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>459</td>
      <td>122</td>
      <td>124134807</td>
      <td>5489</td>
      <td>0.340</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>90358</td>
    </tr>
    <tr>
      <th>127244</th>
      <td>176.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>464</td>
      <td>10</td>
      <td>118739657</td>
      <td>5340</td>
      <td>0.243</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>77171</td>
    </tr>
    <tr>
      <th>127282</th>
      <td>173.0</td>
      <td>2016-03-10</td>
      <td>2016-03-28</td>
      <td>3</td>
      <td>6</td>
      <td>5</td>
      <td>NaN</td>
      <td>2016-03-13</td>
      <td>296</td>
      <td>159</td>
      <td>119888276</td>
      <td>5659</td>
      <td>0.079</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>76341</td>
    </tr>
    <tr>
      <th>127301</th>
      <td>NaN</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>2016-03-02</td>
      <td>105</td>
      <td>87</td>
      <td>118814725</td>
      <td>5239</td>
      <td>0.222</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>10992</td>
    </tr>
    <tr>
      <th>127329</th>
      <td>788.0</td>
      <td>2016-07-15</td>
      <td>2016-07-30</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>NaN</td>
      <td>2016-07-17</td>
      <td>715</td>
      <td>16</td>
      <td>129732490</td>
      <td>5660</td>
      <td>0.143</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>42852</td>
    </tr>
    <tr>
      <th>127372</th>
      <td>147.0</td>
      <td>2016-02-16</td>
      <td>2016-11-08</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>593</td>
      <td>136</td>
      <td>118745109</td>
      <td>5260</td>
      <td>0.394</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>70382</td>
    </tr>
    <tr>
      <th>127430</th>
      <td>NaN</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>2016-03-05</td>
      <td>41</td>
      <td>104</td>
      <td>119134510</td>
      <td>5540</td>
      <td>0.040</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>11313</td>
    </tr>
    <tr>
      <th>127460</th>
      <td>613.0</td>
      <td>2016-05-19</td>
      <td>2016-06-03</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>NaN</td>
      <td>2016-06-02</td>
      <td>113</td>
      <td>116</td>
      <td>126077096</td>
      <td>5895</td>
      <td>0.072</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>96420</td>
    </tr>
    <tr>
      <th>127471</th>
      <td>180.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>784</td>
      <td>130</td>
      <td>118684128</td>
      <td>5170</td>
      <td>0.083</td>
      <td>3</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>9962</td>
    </tr>
    <tr>
      <th>127501</th>
      <td>734.0</td>
      <td>2016-06-19</td>
      <td>2016-07-01</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>NaN</td>
      <td>2016-06-19</td>
      <td>40</td>
      <td>94</td>
      <td>127461259</td>
      <td>5609</td>
      <td>0.154</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>107466</td>
    </tr>
    <tr>
      <th>127554</th>
      <td>NaN</td>
      <td>2016-07-02</td>
      <td>2016-07-11</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>7.0</td>
      <td>2016-07-03</td>
      <td>199</td>
      <td>131</td>
      <td>128383873</td>
      <td>5909</td>
      <td>0.280</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>110332</td>
    </tr>
    <tr>
      <th>127600</th>
      <td>1.0</td>
      <td>2000-01-01</td>
      <td>2030-12-31</td>
      <td>6</td>
      <td>4</td>
      <td>4</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>364</td>
      <td>146</td>
      <td>118628702</td>
      <td>5220</td>
      <td>0.000</td>
      <td>3</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>126724</td>
    </tr>
    <tr>
      <th>127602</th>
      <td>116.0</td>
      <td>2016-12-30</td>
      <td>2030-12-31</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>295</td>
      <td>53</td>
      <td>118659936</td>
      <td>5660</td>
      <td>0.077</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>65471</td>
    </tr>
    <tr>
      <th>127632</th>
      <td>NaN</td>
      <td>2016-03-10</td>
      <td>2016-03-28</td>
      <td>3</td>
      <td>6</td>
      <td>5</td>
      <td>2.0</td>
      <td>2016-03-24</td>
      <td>71</td>
      <td>179</td>
      <td>121075300</td>
      <td>5360</td>
      <td>0.000</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>76930</td>
    </tr>
    <tr>
      <th>127647</th>
      <td>338.0</td>
      <td>2016-04-24</td>
      <td>2016-05-06</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>151</td>
      <td>36</td>
      <td>124126010</td>
      <td>6609</td>
      <td>0.104</td>
      <td>1</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>166734</td>
    </tr>
    <tr>
      <th>127674</th>
      <td>491.0</td>
      <td>2000-01-01</td>
      <td>2016-05-02</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>631</td>
      <td>16</td>
      <td>124122186</td>
      <td>5411</td>
      <td>0.298</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>24243</td>
    </tr>
    <tr>
      <th>127727</th>
      <td>176.0</td>
      <td>2016-02-26</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>NaN</td>
      <td>464</td>
      <td>104</td>
      <td>118628225</td>
      <td>5340</td>
      <td>0.243</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>78526</td>
    </tr>
    <tr>
      <th>127752</th>
      <td>NaN</td>
      <td>2000-01-01</td>
      <td>2030-12-31</td>
      <td>6</td>
      <td>4</td>
      <td>4</td>
      <td>3.0</td>
      <td>2016-07-06</td>
      <td>658</td>
      <td>146</td>
      <td>128506155</td>
      <td>5339</td>
      <td>0.000</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>57167</td>
    </tr>
    <tr>
      <th>127760</th>
      <td>NaN</td>
      <td>2016-06-13</td>
      <td>2016-06-14</td>
      <td>2</td>
      <td>6</td>
      <td>7</td>
      <td>4.0</td>
      <td>2016-06-14</td>
      <td>355</td>
      <td>168</td>
      <td>126996035</td>
      <td>5810</td>
      <td>0.250</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>38005</td>
    </tr>
    <tr>
      <th>127774</th>
      <td>168.0</td>
      <td>2016-02-26</td>
      <td>2016-03-05</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>2.0</td>
      <td>NaN</td>
      <td>130</td>
      <td>124</td>
      <td>118670374</td>
      <td>6359</td>
      <td>0.133</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>3615</td>
    </tr>
    <tr>
      <th>127823</th>
      <td>1.0</td>
      <td>2000-01-01</td>
      <td>2030-12-31</td>
      <td>6</td>
      <td>4</td>
      <td>4</td>
      <td>3.0</td>
      <td>NaN</td>
      <td>637</td>
      <td>136</td>
      <td>118745020</td>
      <td>5310</td>
      <td>0.000</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>56084</td>
    </tr>
    <tr>
      <th>127855</th>
      <td>NaN</td>
      <td>2000-01-01</td>
      <td>2030-12-31</td>
      <td>6</td>
      <td>4</td>
      <td>4</td>
      <td>3.0</td>
      <td>2016-07-01</td>
      <td>790</td>
      <td>135</td>
      <td>128257463</td>
      <td>4870</td>
      <td>0.000</td>
      <td>3</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>125924</td>
    </tr>
  </tbody>
</table>
<p>4387 rows × 18 columns</p>
</div>



## Strategies to replace NA:
- Data: We can replace with the date in the middle between start and end of the spot
- Pubblicita': Find the most frequent "PUBBLICITA" by "TIPO_PUBBLICITA"
- Tipo pubblicita': Find the most frequent "TIPO_PUBBLICITA" by "MACRO_TIPO_PUBBLICITA"

### Replace "Data"


```python
def middle_date(a,b):
    return ((b - a) / 2) + a
def format_date(d) -> str:
    return f'{d.year}-{d.month}-{d.day}'
```


```python
def set_middle_date(row):
    i = row.Index
    start = parse(row.INIZIO_PUBBLICITA)
    end = parse(row.FINE_PUBBLICITA)
    middle = middle_date(start, end)
    dataset.set_value(index=i, col='DATA', value=format_date(middle))
```


```python
data_nulls = dataset[dataset['DATA'].isnull()]
for row in data_nulls.itertuples():
    set_middle_date(row)
```


```python
null_data_after = dataset['DATA'].isnull().any()
print(f"Exists any null data: {null_data_after}")
```

    Exists any null data: False


### Check if exists both null "PUBBLICITA" and "TIPO_PUBBLICITA"


```python
len(dataset[dataset['PUBBLICITA'].isnull() & dataset['TIPO_PUBBLICITA'].isnull()])
```




    0




```python
def most_freq_value_by(row, col, col_by):
    value_col_by = row.__getattribute__(col_by)
    rows_with_value = dataset[dataset[col_by] == value_col_by].dropna()[col]
    mf = rows_with_value.value_counts().max()
    return mf
```

### Replace null "Pubblicita"


```python
def most_frequent_s_by_type_s(row):
    return most_freq_value_by(row, 'PUBBLICITA', 'TIPO_PUBBLICITA')
```


```python
null_spots = dataset[dataset['PUBBLICITA'].isnull()]
for row in null_spots.itertuples():
    i = row.Index
    new_s = most_frequent_s_by_type_s(row)
    dataset.set_value(index=i, col='PUBBLICITA', value=new_s)
```

### Replace null "TIPO_PUBBLICITA"


```python
def most_freq_type_s_by_macro_type_s(row):
    return most_freq_value_by(row, 'TIPO_PUBBLICITA', 'MACRO_TIPO_PUBBLICITA')
```


```python
null_spots = dataset[dataset['TIPO_PUBBLICITA'].isnull()]
for row in null_spots.itertuples():
    i = row.Index
    new_type_s = most_freq_type_s_by_macro_type_s(row)
    dataset.set_value(index=i, col='TIPO_PUBBLICITA', value=new_type_s)
```


```python
exists_null_type_s = dataset['TIPO_PUBBLICITA'].isnull().any()
print(f'Exists any null type spot after replace: {exists_null_type_s}')
```

    Exists any null type spot after replace: False



```python
dataset.isnull().any()
```




    PUBBLICITA                       False
    INIZIO_PUBBLICITA                False
    FINE_PUBBLICITA                  False
    MACRO_TIPO_PUBBLICITA            False
    FASCIA_ORARIA                    False
    FASCIA_TEMPISTICA                False
    TIPO_PUBBLICITA                  False
    DATA                             False
    CODICE_GIOCO                     False
    RILANCIO_PUBBLICITA              False
    SESSIONE                         False
    NUMERO_CLICK_SITO                False
    OFFERTA_PROMOZIONALE             False
    MACRO_CATEGORIA_GIOCO            False
    MICRO_CATEGORIA_GIOCO            False
    AREA_CLICK                       False
    NUMERO_PUBBLICITA_CONCORRENTI    False
    ID                               False
    dtype: bool




```python
dataset.to_csv('../data/interim/01-without_null.csv', index=False)
```

# Correctness data checking

## Data consistency
Check if DATA column is $INIZIO\_PUBBLICITA <= DATA <=  FINE\_PUBBLICITA$ using _dateutil_ python library


```python
#load here to skip null checking phase
dataset = pd.read_csv('../data/interim/01-without_null.csv')
```


```python
def is_data_consistent(row):
    start, end = parse(row['INIZIO_PUBBLICITA']), parse(row['FINE_PUBBLICITA'])
    return start <= parse(row.DATA) <= end
```

### Rows with date inconsistency


```python
inconsistent_dates = ~dataset.apply(is_data_consistent, axis=1)
print(f'#rows with inconsistent dates: {len(dataset[inconsistent_dates])}')
```

    #rows with inconsistent dates: 3361



```python
dataset[inconsistent_dates].head()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PUBBLICITA</th>
      <th>INIZIO_PUBBLICITA</th>
      <th>FINE_PUBBLICITA</th>
      <th>MACRO_TIPO_PUBBLICITA</th>
      <th>FASCIA_ORARIA</th>
      <th>FASCIA_TEMPISTICA</th>
      <th>TIPO_PUBBLICITA</th>
      <th>DATA</th>
      <th>CODICE_GIOCO</th>
      <th>RILANCIO_PUBBLICITA</th>
      <th>SESSIONE</th>
      <th>NUMERO_CLICK_SITO</th>
      <th>OFFERTA_PROMOZIONALE</th>
      <th>MACRO_CATEGORIA_GIOCO</th>
      <th>MICRO_CATEGORIA_GIOCO</th>
      <th>AREA_CLICK</th>
      <th>NUMERO_PUBBLICITA_CONCORRENTI</th>
      <th>ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>103</th>
      <td>79.0</td>
      <td>2016-12-16</td>
      <td>2016-12-18</td>
      <td>2</td>
      <td>6</td>
      <td>3</td>
      <td>2.0</td>
      <td>2016-02-17</td>
      <td>176</td>
      <td>176</td>
      <td>117457236</td>
      <td>6460</td>
      <td>0.500</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>398</td>
    </tr>
    <tr>
      <th>147</th>
      <td>647.0</td>
      <td>2016-06-04</td>
      <td>2016-06-18</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>2016-06-30</td>
      <td>28</td>
      <td>178</td>
      <td>128068299</td>
      <td>5640</td>
      <td>0.170</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>31280</td>
    </tr>
    <tr>
      <th>263</th>
      <td>202.0</td>
      <td>2016-03-10</td>
      <td>2016-03-28</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>5.0</td>
      <td>2016-04-06</td>
      <td>74</td>
      <td>82</td>
      <td>121933117</td>
      <td>5460</td>
      <td>0.165</td>
      <td>2</td>
      <td>1</td>
      <td>20</td>
      <td>4</td>
      <td>144864</td>
    </tr>
    <tr>
      <th>301</th>
      <td>100.0</td>
      <td>2016-12-22</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-02-20</td>
      <td>27</td>
      <td>156</td>
      <td>117793645</td>
      <td>5459</td>
      <td>0.202</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>64895</td>
    </tr>
    <tr>
      <th>310</th>
      <td>140.0</td>
      <td>2016-12-28</td>
      <td>2016-12-31</td>
      <td>8</td>
      <td>6</td>
      <td>8</td>
      <td>4.0</td>
      <td>2016-02-04</td>
      <td>176</td>
      <td>172</td>
      <td>116684483</td>
      <td>7359</td>
      <td>0.179</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>2609</td>
    </tr>
  </tbody>
</table>
</div>



There are many rows, so we use the same approach used with null dates: we put the middle date


```python
rows_i_d = dataset[inconsistent_dates]
for row in rows_i_d.itertuples():
    set_middle_date(row)
```


```python
after_middle_date = ~dataset[inconsistent_dates].apply(is_data_consistent, axis=1)
print(f'Inconsistent dates after set middle date: {after_middle_date.sum()}')
```

    Inconsistent dates after set middle date: 794



```python
end_less_than_start = dataset[inconsistent_dates][after_middle_date]
end_less_than_start.head()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PUBBLICITA</th>
      <th>INIZIO_PUBBLICITA</th>
      <th>FINE_PUBBLICITA</th>
      <th>MACRO_TIPO_PUBBLICITA</th>
      <th>FASCIA_ORARIA</th>
      <th>FASCIA_TEMPISTICA</th>
      <th>TIPO_PUBBLICITA</th>
      <th>DATA</th>
      <th>CODICE_GIOCO</th>
      <th>RILANCIO_PUBBLICITA</th>
      <th>SESSIONE</th>
      <th>NUMERO_CLICK_SITO</th>
      <th>OFFERTA_PROMOZIONALE</th>
      <th>MACRO_CATEGORIA_GIOCO</th>
      <th>MICRO_CATEGORIA_GIOCO</th>
      <th>AREA_CLICK</th>
      <th>NUMERO_PUBBLICITA_CONCORRENTI</th>
      <th>ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>301</th>
      <td>100.0</td>
      <td>2016-12-22</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-7-31</td>
      <td>27</td>
      <td>156</td>
      <td>117793645</td>
      <td>5459</td>
      <td>0.202</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>64895</td>
    </tr>
    <tr>
      <th>378</th>
      <td>100.0</td>
      <td>2016-12-22</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-7-31</td>
      <td>27</td>
      <td>146</td>
      <td>116582326</td>
      <td>5459</td>
      <td>0.202</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>64870</td>
    </tr>
    <tr>
      <th>404</th>
      <td>52.0</td>
      <td>2016-11-21</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-7-15</td>
      <td>536</td>
      <td>32</td>
      <td>116651279</td>
      <td>5509</td>
      <td>0.359</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>63160</td>
    </tr>
    <tr>
      <th>420</th>
      <td>108.0</td>
      <td>2016-12-30</td>
      <td>2016-02-25</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-7-28</td>
      <td>282</td>
      <td>146</td>
      <td>117475562</td>
      <td>5440</td>
      <td>0.050</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>65343</td>
    </tr>
    <tr>
      <th>513</th>
      <td>100.0</td>
      <td>2016-12-22</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-7-31</td>
      <td>27</td>
      <td>128</td>
      <td>119375842</td>
      <td>5459</td>
      <td>0.202</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>64833</td>
    </tr>
  </tbody>
</table>
</div>



### In some cases we have $INIZIO\_PUBBLICITA > FINE\_PUBBLICITA$.
The solution is simple: swap the two columns


```python
cols = list(end_less_than_start)
cols[1], cols[2] = cols[2], cols[1]
end_less_than_start.columns = cols
```


```python
end_less_than_start.head()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PUBBLICITA</th>
      <th>FINE_PUBBLICITA</th>
      <th>INIZIO_PUBBLICITA</th>
      <th>MACRO_TIPO_PUBBLICITA</th>
      <th>FASCIA_ORARIA</th>
      <th>FASCIA_TEMPISTICA</th>
      <th>TIPO_PUBBLICITA</th>
      <th>DATA</th>
      <th>CODICE_GIOCO</th>
      <th>RILANCIO_PUBBLICITA</th>
      <th>SESSIONE</th>
      <th>NUMERO_CLICK_SITO</th>
      <th>OFFERTA_PROMOZIONALE</th>
      <th>MACRO_CATEGORIA_GIOCO</th>
      <th>MICRO_CATEGORIA_GIOCO</th>
      <th>AREA_CLICK</th>
      <th>NUMERO_PUBBLICITA_CONCORRENTI</th>
      <th>ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>301</th>
      <td>100.0</td>
      <td>2016-12-22</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-7-31</td>
      <td>27</td>
      <td>156</td>
      <td>117793645</td>
      <td>5459</td>
      <td>0.202</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>64895</td>
    </tr>
    <tr>
      <th>378</th>
      <td>100.0</td>
      <td>2016-12-22</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-7-31</td>
      <td>27</td>
      <td>146</td>
      <td>116582326</td>
      <td>5459</td>
      <td>0.202</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>64870</td>
    </tr>
    <tr>
      <th>404</th>
      <td>52.0</td>
      <td>2016-11-21</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-7-15</td>
      <td>536</td>
      <td>32</td>
      <td>116651279</td>
      <td>5509</td>
      <td>0.359</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>63160</td>
    </tr>
    <tr>
      <th>420</th>
      <td>108.0</td>
      <td>2016-12-30</td>
      <td>2016-02-25</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-7-28</td>
      <td>282</td>
      <td>146</td>
      <td>117475562</td>
      <td>5440</td>
      <td>0.050</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>65343</td>
    </tr>
    <tr>
      <th>513</th>
      <td>100.0</td>
      <td>2016-12-22</td>
      <td>2016-03-09</td>
      <td>7</td>
      <td>1</td>
      <td>1</td>
      <td>4.0</td>
      <td>2016-7-31</td>
      <td>27</td>
      <td>128</td>
      <td>119375842</td>
      <td>5459</td>
      <td>0.202</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>64833</td>
    </tr>
  </tbody>
</table>
</div>




```python
inconsistent_after_2 = ~end_less_than_start.apply(is_data_consistent, axis=1)
print(f'Inconsistent dates after swapping: {inconsistent_after_2.sum()}')
```

    Inconsistent dates after swapping: 0



```python
for row in end_less_than_start.itertuples():
    i = row.Index
    start, end = row.INIZIO_PUBBLICITA, row.FINE_PUBBLICITA
    dataset.set_value(index=i, col='INIZIO_PUBBLICITA', value=start)
    dataset.set_value(index=i, col='FINE_PUBBLICITA', value=end)
```

### Final check of inconsistent dates


```python
inconsistent_dates = ~dataset.apply(is_data_consistent, axis=1)
```


```python
print(f'Inconsistent dates after solving: {inconsistent_dates.sum()}')
```

    Inconsistent dates after solving: 0



```python
dataset.to_csv('../data/interim/02-solved-inconsistent-dates.csv', index=False)
```

### (Post model) Checking insensate years


```python
%matplotlib inline
dataset = pd.read_csv('../data/interim/02-solved-inconsistent-dates.csv')
```


```python
dataset.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 127888 entries, 0 to 127887
    Data columns (total 18 columns):
    PUBBLICITA                       127888 non-null float64
    INIZIO_PUBBLICITA                127888 non-null object
    FINE_PUBBLICITA                  127888 non-null object
    MACRO_TIPO_PUBBLICITA            127888 non-null int64
    FASCIA_ORARIA                    127888 non-null int64
    FASCIA_TEMPISTICA                127888 non-null int64
    TIPO_PUBBLICITA                  127888 non-null float64
    DATA                             127888 non-null object
    CODICE_GIOCO                     127888 non-null int64
    RILANCIO_PUBBLICITA              127888 non-null int64
    SESSIONE                         127888 non-null int64
    NUMERO_CLICK_SITO                127888 non-null int64
    OFFERTA_PROMOZIONALE             127888 non-null float64
    MACRO_CATEGORIA_GIOCO            127888 non-null int64
    MICRO_CATEGORIA_GIOCO            127888 non-null int64
    AREA_CLICK                       127888 non-null int64
    NUMERO_PUBBLICITA_CONCORRENTI    127888 non-null int64
    ID                               127888 non-null int64
    dtypes: float64(3), int64(12), object(3)
    memory usage: 17.6+ MB



```python
dataset.describe()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PUBBLICITA</th>
      <th>MACRO_TIPO_PUBBLICITA</th>
      <th>FASCIA_ORARIA</th>
      <th>FASCIA_TEMPISTICA</th>
      <th>TIPO_PUBBLICITA</th>
      <th>CODICE_GIOCO</th>
      <th>RILANCIO_PUBBLICITA</th>
      <th>SESSIONE</th>
      <th>NUMERO_CLICK_SITO</th>
      <th>OFFERTA_PROMOZIONALE</th>
      <th>MACRO_CATEGORIA_GIOCO</th>
      <th>MICRO_CATEGORIA_GIOCO</th>
      <th>AREA_CLICK</th>
      <th>NUMERO_PUBBLICITA_CONCORRENTI</th>
      <th>ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>1.278880e+05</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.0</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
      <td>127888.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>387.274842</td>
      <td>5.816378</td>
      <td>2.509805</td>
      <td>2.776203</td>
      <td>130.158678</td>
      <td>392.297940</td>
      <td>94.743635</td>
      <td>1.235237e+08</td>
      <td>5505.730616</td>
      <td>0.156185</td>
      <td>1.986535</td>
      <td>1.0</td>
      <td>5.113865</td>
      <td>1.733009</td>
      <td>83920.230686</td>
    </tr>
    <tr>
      <th>std</th>
      <td>1154.617967</td>
      <td>1.835892</td>
      <td>1.917565</td>
      <td>2.181093</td>
      <td>2137.918359</td>
      <td>221.969084</td>
      <td>51.401158</td>
      <td>4.188625e+06</td>
      <td>579.002292</td>
      <td>0.136981</td>
      <td>0.454355</td>
      <td>0.0</td>
      <td>15.235427</td>
      <td>3.506405</td>
      <td>48428.294332</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.163034e+08</td>
      <td>-100.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>1.0</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>119.000000</td>
      <td>6.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>3.000000</td>
      <td>199.000000</td>
      <td>53.000000</td>
      <td>1.198935e+08</td>
      <td>5340.000000</td>
      <td>0.000000</td>
      <td>2.000000</td>
      <td>1.0</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>41950.750000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>211.000000</td>
      <td>7.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>4.000000</td>
      <td>440.000000</td>
      <td>99.000000</td>
      <td>1.236580e+08</td>
      <td>5472.000000</td>
      <td>0.149000</td>
      <td>2.000000</td>
      <td>1.0</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>84000.500000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>625.000000</td>
      <td>7.000000</td>
      <td>4.000000</td>
      <td>4.000000</td>
      <td>5.000000</td>
      <td>595.000000</td>
      <td>142.000000</td>
      <td>1.268844e+08</td>
      <td>5660.000000</td>
      <td>0.243000</td>
      <td>2.000000</td>
      <td>1.0</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>125822.250000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>28256.000000</td>
      <td>8.000000</td>
      <td>6.000000</td>
      <td>8.000000</td>
      <td>41368.000000</td>
      <td>807.000000</td>
      <td>181.000000</td>
      <td>1.349159e+08</td>
      <td>15050.000000</td>
      <td>2.778000</td>
      <td>3.000000</td>
      <td>1.0</td>
      <td>115.000000</td>
      <td>37.000000</td>
      <td>167839.000000</td>
    </tr>
  </tbody>
</table>
</div>




```python
def irrealistic_year(year):
    return not(1950 <= year <= 2018)
def get_year(str_date):
    return int(str_date[:4])
def irrealistic_year_row(row):
    return any(irrealistic_year(get_year(row[datecol])) for datecol in DATECOLS)
def middle_date(a,b):
    return ((b - a) / 2) + a
def format_date(d) -> str:
    return f'{d.year}-{d.month}-{d.day}'
```


```python
bad_start = dataset[dataset.INIZIO_PUBBLICITA.apply(get_year).apply(irrealistic_year)]
bad_date = dataset[dataset.DATA.apply(get_year).apply(irrealistic_year)]
bad_end = dataset[dataset.FINE_PUBBLICITA.apply(get_year).apply(irrealistic_year)]
```


```python
bad_start.INIZIO_PUBBLICITA.apply(get_year).describe()
```




    count    2916.0
    mean     1900.0
    std         0.0
    min      1900.0
    25%      1900.0
    50%      1900.0
    75%      1900.0
    max      1900.0
    Name: INIZIO_PUBBLICITA, dtype: float64




```python
bad_start.INIZIO_PUBBLICITA.apply(get_year).sort_values().hist(bins=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7f0cb176b780>




![png](output_56_1.png)



```python
bad_date.DATA.apply(get_year).sort_values().plot('hist', xticks=[2018, 3000])
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7f0ca7ab89e8>




![png](output_57_1.png)



```python
bad_end.FINE_PUBBLICITA.apply(get_year).sort_values().plot('hist', xticks=[2018])
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7f0ca5f67630>




![png](output_58_1.png)


## Solve irrealistic year:
- If start and end year are irrealistic then set to year data if consistent
- If data is inconsistent set data to the middle of start and end
- If only start is irrealistic set start to data
- If only end is irrealistic set end to data
- Otherwise delete the row


```python
def solve_irrealistic_date(row):
    irrealistic_start = irrealistic_year(get_year(row['INIZIO_PUBBLICITA']))
    irrealistic_end = irrealistic_year(get_year(row['FINE_PUBBLICITA']))
    irrealistic_date = irrealistic_year(get_year(row['DATA']))
    if irrealistic_start and irrealistic_end and not irrealistic_date:
        row['INIZIO_PUBBLICITA'] =  row['DATA']
        row['FINE_PUBBLICITA'] = row['DATA']
    elif irrealistic_date and not irrealistic_start and not irrealistic_end:
        a, b = parse(row['INIZIO_PUBBLICTA']), parse(row['FINE_PUBBLICITA'])
        middle = middle_date(a,b)
        row['DATA'] =  format_date(middle)
    elif irrealistic_start and not irrealistic_end and not irrealistic_date:
        row['INIZIO_PUBBLICITA'] = row['DATA']
    elif irrealistic_end and not irrealistic_start and not irrealistic_date:
        row['FINE_PUBBLICITA'] = row['DATA']
    return row
```


```python
dataset = dataset.apply(solve_irrealistic_date, axis=1)
dataset = dataset[~dataset.apply(irrealistic_year_row, axis=1)]
```


```python
bad_start = dataset[dataset.INIZIO_PUBBLICITA.apply(get_year).apply(irrealistic_year)]
bad_end = dataset[dataset.FINE_PUBBLICITA.apply(get_year).apply(irrealistic_year)]
bad_date = dataset[dataset.DATA.apply(get_year).apply(irrealistic_year)]
```


```python
len(bad_start) + len(bad_end) + len(bad_date)
```




    0



## Invalid values on 'NUMERO_CLICK_SITO'
Simply remove the rows when the variable to predict is negative. In this case, because this is the variable to predict we can't replace the negative values with the mean of similar others


```python
dataset = dataset[dataset.NUMERO_CLICK_SITO > 0]
```


```python
dataset.to_csv('../data/interim/02-01-solved-irrealistic-year-negative-predvar.csv', index=False)
```
