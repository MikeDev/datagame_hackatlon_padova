from sklearn.ensemble import RandomForestRegressor
import pandas as pd
from path import Path
import datetime

prj_root = Path(__file__).parent.parent.parent
processed_fold = prj_root / 'data' / 'processed'
rawtest = processed_fold.parent / 'raw' / 'test.csv'
finaltrain = processed_fold / 'finaltrain.csv'
finaltest = processed_fold / 'finaltest.csv'
submissions = prj_root / 'predictions'


def predict(X_train, y_train, X_test):
    rf = RandomForestRegressor(n_estimators=200, random_state=13, n_jobs=-1, min_samples_split=20)
    rf2 = RandomForestRegressor(n_jobs=-1, random_state=13, min_samples_split=60)
    rf2.fit(X_train, y_train)
    rf.fit(X_train, y_train)
    pred1 =  rf.predict(X_test)
    pred2 = rf2.predict(X_test)
    pred = ((pred1 + pred2) / 2).round().astype('int')
    return pred



def group_test(rawtest, pred_y):
    to_group = ["PUBBLICITA", "CODICE_GIOCO", "MACRO_TIPO_PUBBLICITA",
                "FASCIA_TEMPISTICA", "FASCIA_ORARIA", "TIPO_PUBBLICITA",
                "MICRO_CATEGORIA_GIOCO", "RILANCIO_PUBBLICITA",
                "AREA_CLICK", "DATA"]
    rawtest['NUMERO_CLICK_SITO'] = pred_y
    agg_dict = {col: 'max' for col in rawtest.columns}
    agg_dict['ID'] = 'max'
    agg_dict['NUMERO_CLICK_SITO'] = 'sum'
    grouped: pd.DataFrame = rawtest.groupby(to_group).agg(agg_dict)
    return grouped


def load_train_test(trainpath, testpath):
    train = pd.read_csv(trainpath)
    test = pd.read_csv(testpath)
    X_train, y_train = train.drop(['ID', 'NUMERO_CLICK_SITO'], axis=1).as_matrix(), train['NUMERO_CLICK_SITO']
    X_test = test.drop('ID', axis=1).as_matrix()
    return X_train, y_train, X_test


def get_submission(trainpath, testpath, rawtestpath):
    X_train, y_train, X_test = load_train_test(trainpath, testpath)
    pred_y = predict(X_train, y_train, X_test)

    rawtest = pd.read_csv(rawtestpath)
    grouped = group_test(rawtest, pred_y)
    submission = grouped[['ID', 'NUMERO_CLICK_SITO']]
    return submission


def main():

    sub = get_submission(finaltrain, finaltest, rawtest)
    strdatetime = str(datetime.datetime.utcnow()).replace(" ", "_").replace(':', "-")
    sub.to_csv(submissions / 'prediction_' + strdatetime , sep=' ', index=False, header=False)
    #TODO save file with actual datetime (find func now())


if __name__ == '__main__':
    main()
