# -*- coding: utf-8 -*-
import os
import click
import logging
from dotenv import find_dotenv, load_dotenv
from path import Path
from features import build_features, date_consistency, delete_errors
import pandas as pd


def build_dataset(datapath, test=False):
    dataset = pd.read_csv(datapath)
    dataset = delete_errors.fix_nulls(dataset)
    if not test:
        dataset = delete_errors.fix_negative_predvar(dataset)
    dataset = date_consistency.fix_date_consistency(dataset)
    dataset = date_consistency.fix_insensate_years(dataset)
    dataset = build_features.build(dataset)
    return dataset


def main():
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    prj_root = Path(__file__).parent.parent.parent
    raw_folder = prj_root / 'data' / 'raw'
    processed_fold  = prj_root / 'data' / 'processed'
    logger = logging.getLogger(__name__)
    logger.info('making final train set from raw data')

    trainpath = raw_folder / 'train.csv'
    finaltrain = processed_fold / 'finaltrain.csv'
    train = build_dataset(trainpath)
    train.to_csv(finaltrain, index=False)

    logger.info('making final test set from raw data')


    testpath = raw_folder / 'test.csv'
    finaltest = processed_fold / 'finaltest.csv'
    test = build_dataset(testpath, test=True)
    test.to_csv(finaltest, index=False)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
