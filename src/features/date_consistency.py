from dateutil.parser import parse
from delete_errors import set_middle_date

DATECOLS = ['INIZIO_PUBBLICITA', 'FINE_PUBBLICITA', 'DATA']


def is_data_consistent(row):
    start, end = parse(row['INIZIO_PUBBLICITA']), parse(row['FINE_PUBBLICITA'])
    return start <= parse(row.DATA) <= end


def fix_date_consistency(dataset):
    inconsistent_dates = ~dataset.apply(is_data_consistent, axis=1)
    if len(inconsistent_dates) > 0:
        rows_i_d = dataset[inconsistent_dates]
        for row in rows_i_d.itertuples():
            set_middle_date(dataset, row)
        after_middle_date = ~dataset[inconsistent_dates].apply(is_data_consistent, axis=1)
        if len(after_middle_date) > 0:
            end_less_than_start = dataset[inconsistent_dates][after_middle_date]
            cols = list(end_less_than_start)
            cols[1], cols[2] = cols[2], cols[1]
            end_less_than_start.columns = cols
            for row in end_less_than_start.itertuples():
                i = row.Index
                start, end = row.INIZIO_PUBBLICITA, row.FINE_PUBBLICITA
                dataset.set_value(index=i, col='INIZIO_PUBBLICITA', value=start)
                dataset.set_value(index=i, col='FINE_PUBBLICITA', value=end)
    return dataset


def irrealistic_year(year):
    return not (1950 <= year <= 2018)


def get_year(str_date):
    return int(str_date[:4])


def irrealistic_year_row(row):
    return any(irrealistic_year(get_year(row[datecol])) for datecol in DATECOLS)


def middle_date(a, b):
    return ((b - a) / 2) + a


def format_date(d) -> str:
    return f'{d.year}-{d.month}-{d.day}'


def solve_irrealistic_date(row):
    irrealistic_start = irrealistic_year(get_year(row['INIZIO_PUBBLICITA']))
    irrealistic_end = irrealistic_year(get_year(row['FINE_PUBBLICITA']))
    irrealistic_date = irrealistic_year(get_year(row['DATA']))
    if irrealistic_start and irrealistic_end and not irrealistic_date:
        row['INIZIO_PUBBLICITA'] = row['DATA']
        row['FINE_PUBBLICITA'] = row['DATA']
    elif irrealistic_date and not irrealistic_start and not irrealistic_end:
        a, b = parse(row['INIZIO_PUBBLICTA']), parse(row['FINE_PUBBLICITA'])
        middle = middle_date(a, b)
        row['DATA'] = format_date(middle)
    elif irrealistic_start and not irrealistic_end and not irrealistic_date:
        row['INIZIO_PUBBLICITA'] = row['DATA']
    elif irrealistic_end and not irrealistic_start and not irrealistic_date:
        row['FINE_PUBBLICITA'] = row['DATA']
    return row


def fix_insensate_years(dataset):
    dataset = dataset.apply(solve_irrealistic_date, axis=1)
    dataset = dataset[~dataset.apply(irrealistic_year_row, axis=1)]
    return dataset
