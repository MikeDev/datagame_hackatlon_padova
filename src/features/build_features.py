from dateutil.parser import parse
from sklearn.decomposition import PCA
import pandas as pd


def extract_day_month_year(dataset, date_column: str):
    dates = list(map(parse, dataset[date_column]))
    days, months, years = map(lambda date: date.day, dates), map(lambda date: date.month, dates), map(
        lambda date: date.year, dates)
    return ("GIORNO_" + date_column, days), \
           ("MESE_" + date_column, months), \
           ("ANNO_" + date_column, years)


def getweekday(datecol):
    if type(datecol) != str:
        print(datecol)
    return parse(datecol).weekday()


def extract_weekdays(dataset):
    datecols = ['DATA']
    weekdays = ['LUN', 'MAR', 'MER', 'GIO', 'VEN', 'SAB', 'DOM']
    dict_weekdays = {i: weekday for i, weekday in enumerate(weekdays)}
    for datecol in datecols:
        weekdaycol = dataset[datecol].apply(getweekday)
        days = weekdaycol.apply(dict_weekdays.get)
        dummies = pd.get_dummies(days, prefix=datecol + '_')
        dataset = pd.concat([dataset, dummies], axis=1)
    return dataset

# unuseful doesn't change anything
# def dummy_macro_categoria_gioco(dataset):
#     dummies = pd.get_dummies(dataset['MACRO_CATEGORIA_GIOCO'], prefix='MACRO_CATEGORIA_GIOCO_')
#     dataset = pd.concat([dataset, dummies], axis=1)
#     del dataset['MACRO_CATEGORIA_GIOCO']
#     return dataset

def build(dataset):
    date_columns = ["INIZIO_PUBBLICITA", "FINE_PUBBLICITA", "DATA"]
    for date_col in date_columns:
        newcols = extract_day_month_year(dataset, date_col)
        for newcolname, values in newcols:
            dataset[newcolname] = list(values)
    dataset = extract_weekdays(dataset)
    # str vars
    del dataset['DATA']
    del dataset['INIZIO_PUBBLICITA']
    del dataset['FINE_PUBBLICITA']
    # correlation
    del dataset['ANNO_INIZIO_PUBBLICITA']
    del dataset['MESE_INIZIO_PUBBLICITA']
    del dataset['GIORNO_INIZIO_PUBBLICITA']
    del dataset['MESE_DATA']
    pca = PCA(n_components=1)
    dataset['FASCIA_PCA'] = pca.fit_transform(dataset[['FASCIA_ORARIA', 'FASCIA_TEMPISTICA']])
    dataset = dataset.drop(['FASCIA_ORARIA', 'FASCIA_TEMPISTICA'], axis=1)
    return dataset
