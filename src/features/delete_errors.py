from dateutil.parser import parse


def middle_date(a, b):
    return ((b - a) / 2) + a


def format_date(d) -> str:
    return f'{d.year}-{d.month}-{d.day}'


def set_middle_date(dataset, row):
    i = row.Index
    start = parse(row.INIZIO_PUBBLICITA)
    end = parse(row.FINE_PUBBLICITA)
    middle = middle_date(start, end)
    dataset.set_value(index=i, col='DATA', value=format_date(middle))


def most_freq_value_by(dataset, row, col, col_by):
    value_col_by = row.__getattribute__(col_by)
    rows_with_value = dataset[dataset[col_by] == value_col_by].dropna()[col]
    mf = rows_with_value.value_counts().max()
    return mf


def most_frequent_s_by_type_s(dataset, row):
    return most_freq_value_by(dataset, row, 'PUBBLICITA', 'TIPO_PUBBLICITA')


def most_freq_type_s_by_macro_type_s(dataset, row):
    return most_freq_value_by(dataset, row, 'TIPO_PUBBLICITA', 'MACRO_TIPO_PUBBLICITA')


def fix_negative_predvar(dataset):
    return dataset[dataset.NUMERO_CLICK_SITO > 0]


def fix_nulls(dataset):
    data_nulls = dataset[dataset['DATA'].isnull()]
    for row in data_nulls.itertuples():
        set_middle_date(dataset, row)

    null_spots = dataset[dataset['PUBBLICITA'].isnull()]
    for row in null_spots.itertuples():
        i = row.Index
        new_s = most_frequent_s_by_type_s(dataset, row)
        dataset.set_value(index=i, col='PUBBLICITA', value=new_s)

    null_spots = dataset[dataset['TIPO_PUBBLICITA'].isnull()]
    for row in null_spots.itertuples():
        i = row.Index
        new_type_s = most_freq_type_s_by_macro_type_s(dataset, row)
        dataset.set_value(index=i, col='TIPO_PUBBLICITA', value=new_type_s)
    return dataset
