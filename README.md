datagame_hackatlon
==============================

DataGame Hackatlon hosted by Miriade at Padova

##Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── predictions         <- Where the prediction of the model are saved
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── src                <- Source code for use in this project.
    │   │
    │   ├── data
    │   │   └── make_dataset.py  <────── Preprocess the test and train set
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   ├── date_consistency.py   <─── Resolve wrong dates
    │   │   ├── delete_errors.py      <─── Resolve null values and negative values of variable to predict
    │   │   └── build_features.py     <─── Create and remove the features
    │   │
    │   └── models         <- Scripts to train models and then use trained models to make
    │       │                 predictions
    │       ├── predict_model.py
    │       └── train_model.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


--------

------------------------------------
## Dataset description (italian)
I dati, prontamente mascherati, sono relativi alle registrazioni del numero di click (eventi) di un sito internet; sono un sottocampione di tutti i dati registrati nell’arco di 6 mesi di attività per il settore videogiochi sui quali sono state fatte aggregazioni e rielaborazioni.

L’obiettivo è definire un modello per la previzione del numero di click (NUMERO_CLICK_SITO)

I dataset sono:

- dataset di train con 127888 righe
- dataset di test con 40889 righe – dati su cui fare la previsione

Le variabili sono:
- IDRIGA: id di riga univoco utilizzato per il calcolo dello score
- PUBBLICITA : Id della pubblicità
- INIZIO_PUBBLICITA : data di inizio pubblicità
- FINE_PUBBLICITA : data di fine pubblicità
- MACRO_TIPO_PUBBLICITA : canale del tipo di pubblicità
- FASCIA_ORARIA : fascia oraria di lancio della pubblicità
- FASCIA_TEMPISTICA : classe di durata temporale della pubblicità
- TIPO_PUBBLICITA : tipologia della pubblicità
- DATA : data registrazione eventid
- CODICE_GIOCO : gioco presente nella pagina web
- RILANCIO_PUBBLICITA : quante volte la pubblicità è stata rimandata prima di quella data
- SESSIONE : sessione di registrazione dei click
- NUMERO_CLICK_SITO : numero di click registrati
- OFFERTA_PROMOZIONALE : offerta promozionale lanciata durante la pubblicità
- MACRO_CATEGORIA_GIOCO : categoria macro prodotto
- MICRO_CATEGORIA_GIOCO : categoria micro prodotto
- AREA_CLICK : click per area
- NUMERO_PUBBLICITA_CONCORRENTI: numero delle pubblicità/offerte promozionali presenti nei canali di comunicazione nel periodo inizio-fine pubblicità

Deve essere effettuato un controllo sulla DATA, infatti la DATA deve essere compresa tra INIZIO_PUBBLICITA e FINE_PUBBLICITA.

I dati devono essere aggregati/raggruppati per:
- “PUBBLICITA”
- “CODICE_GIOCO”
- “MACRO_TIPO_PUBBLICITA”
- “FASCIA_TEMPISTICA”
- “FASCIA_ORARIA”
- “TIPO_PUBBLICITA”
- “MICRO_CATEGORIA_GIOCO”
- “RILANCIO_PUBBLICITA”
- “AREA_CLICK”
- “DATA”
considerando una misura (somma, massimo, minimo, media) per le altre variabili inserite nell’analisi. La nuova variabile risposta diventa quindi la somma del NUMERO_CLICK_SITO delle osservazioni in ogni strato (aggregazione).
IMPORTANTE: E’ necessario inoltre prendere il massimo della variabile ID. Questo ID costituisce l’ID per l’identificazione della osservazione corrispondente nel dataset di verifica (si veda anche il TUTORIAL).

Per effettuare il raggruppamento è consigliabile utilizzare librerie R come dplyr (group_by per l’aggregazione e summarise per le misure delle altre variabili e per il massimo di ID) o moduli python come pandas.DataFrame (groupby per l’aggregazione e agg per le altre variabili e per il massimo di ID).


